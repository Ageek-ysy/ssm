package com.ageeklet.ssm.web.controller;


import com.ageeklet.ssm.entity.Student;
import com.ageeklet.ssm.service.StudentService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class StudentController implements Controller {

    private StudentService service ;

    public void setService(StudentService service) {
        this.service = service;
    }

    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String name = request.getParameter("name");
        String ageStr = request.getParameter("age");
        Integer age = Integer.valueOf(ageStr);
        Student student = new Student(name,age);

        service.addStudent(student);

        return new ModelAndView("/WEB-INF/views/index.jsp");
    }

}
