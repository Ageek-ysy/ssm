package com.ageeklet.ssm.dao;

import com.ageeklet.ssm.entity.Student;

public interface StudentDao {

    void insertStudent(Student student);

}
