package com.ageeklet.ssm.service.impl;

import com.ageeklet.ssm.dao.StudentDao;
import com.ageeklet.ssm.entity.Student;
import com.ageeklet.ssm.service.StudentService;

public class StudentServiceImpl implements StudentService {

    private StudentDao dao;

    public void setDao(StudentDao dao) {
        this.dao = dao;
    }

    @Override
    public void addStudent(Student student) {
        dao.insertStudent(student);
    }
}
