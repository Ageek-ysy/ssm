package com.ageeklet.ssm.service;

import com.ageeklet.ssm.entity.Student;

public interface StudentService {

    void addStudent(Student student);

}
